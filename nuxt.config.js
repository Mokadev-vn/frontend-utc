export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  env: {
    API_BASE_URL: process.env.API_BASE_URL,
  },
  static: {
    prefix: false
  },
  head: {
    title: "Home Page",
    htmlAttrs: {
      lang: "en"
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" }
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
    script: [
      { src: process.env.BASE_URL + "js/jquery-3.3.1.js" },
      { src: process.env.BASE_URL + "js/jquery-migrate-3.0.0.min.js" },
      { src: process.env.BASE_URL + "js/popper.min.js" },
      { src: process.env.BASE_URL + "js/bootstrap.min.js" },
      { src: process.env.BASE_URL + "js/jquery.mmenu.all.js" },
      { src: process.env.BASE_URL + "js/ace-responsive-menu.js" },
      { src: process.env.BASE_URL + "js/chart.min.js" },
      { src: process.env.BASE_URL + "js/bootstrap-select.min.js" },
      { src: process.env.BASE_URL + "js/snackbar.min.js" },
      { src: process.env.BASE_URL + "js/parallax.js" },
      { src: process.env.BASE_URL + "js/scrollto.js" },
      { src: process.env.BASE_URL + "js/jquery-scrolltofixed-min.js" },
      { src: process.env.BASE_URL + "js/jquery.counterup.js" },
      { src: process.env.BASE_URL + "js/wow.min.js" },
      { src: process.env.BASE_URL + "js/progressbar.js" },
      { src: process.env.BASE_URL + "js/slider.js" },
      { src: process.env.BASE_URL + "js/timepicker.js" },
      { src: process.env.BASE_URL + "js/script.js" }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    "./static/css/bootstrap.min.css",
    "./static/css/style.css",
    "./static/css/responsive.css"
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: "./plugins/notification.js", mode: "client" },
    { src: "./plugins/vee-validate" },
    { src: "./plugins/tinymce", ssr: false },
    { src: "./plugins/modal.js" },
    { src: "./plugins/slider.js", ssr: false }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: ["@nuxtjs/axios", "@nuxtjs/auth-next", "vue-sweetalert2/nuxt"],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    babel: {
      plugins: [["@babel/plugin-proposal-private-methods", { loose: true }]]
    }
  },
  router: {
    linkExactActiveClass: "active"
  },
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    baseURL: process.env.API_BASE_URL,
    headers: {
      common: {
        Accept: "application/json"
      }
    }
  },
  vue: {
    config: {
      productionTip: false,
      devtools: true
    }
  },
  auth: {
    localStorage: {
      prefix: "utc_"
    }
  }
};
