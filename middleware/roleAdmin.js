export default function({ app, router, redirect }) {
  app.$auth.$storage.syncUniversal("infoUser");
  const infoUser = app.$auth.$storage.getState("infoUser");
  if (!infoUser) {
    return redirect("/login");
  }
  if (infoUser.codeRole != "ADMIN") {
    return redirect("/dashboard");
  }
  return true;
}
