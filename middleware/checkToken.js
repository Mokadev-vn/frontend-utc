export default function({ app, router, redirect }) {
  const infoUser = app.$auth.$storage.getCookie("infoUser");
  if (!infoUser) {
    return redirect("/login");
  }
  try {
    app.$axios.setToken(infoUser.token, "Bearer");
    app.$axios
      .post("api/user/profile", {})
      .then(data => {
        return true;
      })
      .catch(error => {
        app.$auth.$storage.removeUniversal("infoUser");
        return redirect("/login");
      });
  } catch (e) {
    app.$auth.$storage.removeUniversal("infoUser");
    return redirect("/login");
  }
}
