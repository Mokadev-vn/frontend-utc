export default {
  async getAll(axios) {
    const response = await axios.get("/api/support/get-all");
    return response.data;
  },
  async getOne(data, axios){
    const response = await axios.get(`/api/support/${data}`)
    return response.data
  },
  async delete(data, axios) {
    const response = await axios.delete(`/api/support/${data}`);
    return response.data;
  },
  async sendMail(data, axios) {
      const response = await axios.post('/api/support/send-email', data)
      return response.data
  }
};
