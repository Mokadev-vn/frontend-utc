export default {
    async createPost(data, axios){
        const response = await axios.post('/api/post', data);
        return response.data
    },
    async getPost(data, axios){
        const response = await axios.post('/api/post/get-all', data);
        return response.data
    },
    async getPostVisitor(data, axios){
        const response = await axios.post('/api/post/get-all-visitor', data);
        return response.data
    },
    async getOnePost(data, axios){
        const response = await axios.get(`/api/post/${data}`);
        return response.data
    },
    async getOnePostVisitor(data, axios){
        const response = await axios.post('/api/post/get-detail-visitor', data);
        return response.data
    },
    async deletePost(data, axios){
        const response = await axios.delete(`/api/post/${data}`);
        return response.data
    },

    async createCategory(data, axios){
        const response = await axios.post('/api/post-categories', data);
        return response.data
    },
    async getCategory(axios){
        const response = await axios.get('/api/post-categories/get-all');
        return response.data
    },
    async getCategoryVisitor(axios){
        const response = await axios.get('/api/post-categories/get-all-visitor');
        return response.data
    },
    async getOneCategory(data, axios){
        const response = await axios.get(`/api/post-categories/${data}`);
        return response.data
    },
    async deleteCategory(data, axios){
        const response = await axios.delete(`/api/post-categories/${data}`);
        return response.data
    },
    async getPostToCategory(data, axios){
        const response = await axios.post(`/api/post-categories/get-detail-visitor`, data);
        return response.data
    }
}