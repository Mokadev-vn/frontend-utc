export default {
  // lấy tấy cả user
  async getListUser(axios) {
    const response = await axios.get("/api/users");
    return response.data;
  },
  // lấy tất cả quyền
  async getListRole(axios) {
    const response = await axios.get("/api/roles");
    return response.data;
  },
  // thay đổi quyền user
  async changeRole(data, axios) {
    const response = await axios.post("/api/user/update-role", data);
    return response.data;
  },
  // thay đổi status user
  async changeStatus(data, axios) {
    const response = await axios.post("/api/user/update-status", data);
    return response.data;
  },
  // lấy thông tin bảng báo cáo
  async report(axios) {
    const response = await axios.post("/api/report");
    return response.data;
  },
  // sửa thông tin id
  async editCompany(data, axios) {
    const response = await axios.post("/api/infomation", data);
    return response.data;
  },
  // lấy thông tin của công ty
  async getCompany(data, axios) {
    const response = await axios.get(`/api/infomation/${data}`);
    return response.data;
  },
  async getInfomation(axios){
    const response = await axios.get('/api/infomation/get-detail-visitor')
    return response.data
  },
  // thêm menu
  async postMenu(data, axios) {
    const response = await axios.post('/api/navbar', data);
    return response.data;
  },
  // lấy tất cả menu
  async getMenu(data, axios) {
    const response = await axios.post("/api/navbar/get-all", data);
    return response.data;
  },
  // lấy thông tin 1 menu
  async getOneMenu(data, axios) {
      const response = await axios.get(`/api/navbar/${data}`)
      return response.data
  },
  // xóa menu
  async deleteMenu(data, axios) {
    const response = await axios.delete(`/api/navbar/${data}`);
    return response.data;
  },
  // lấy tất cả description
  async getDescription(data, axios) {
    const response = await axios.post(`/api/description/get-all`, data)
    return response.data
  },
  async getOneDescription(data, axios) {
    const response = await axios.get(`/api/description/${data}`)
    return response.data
  },
  async postDescription(data, axios) {
    const response = await axios.post(`/api/description`, data)
    return response.data
  },
  async deleteDescription(data, axios){
    const response = await axios.delete(`/api/description/${data}`)
    return response.data
  }
};
