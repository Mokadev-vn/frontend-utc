export default {
    async getMenu(data, axios){
        const response = await axios.post('/api/navbar/get-menus',data);
        return response.data
    }
}