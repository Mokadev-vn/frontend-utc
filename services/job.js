export default {
    async list(data, axios){
        const response = await axios.post('api/job/get-all-visitor', data);
        return response.data
    },
    async postJob(data, axios){
        const response = await axios.post('api/job', data);
        return response.data
    },
    async getOne(data, axios){
        const response = await axios.get(`api/job/${data}`)
        return response.data
    },
    async getOneDetails(data, axios){
        const response = await axios.post('api/job/get-detail-visitor', data)
        return response.data
    },
    async updateJob(data, axios){
        const response = await axios.post('api/job', data)
        return response.data
    },
    async delete(data, axios){
        const response = await axios.delete(`api/job/${data}`)
        return response.data
    }
}