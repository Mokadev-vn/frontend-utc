export default {
    async postContact(data, axios) {
        const response = await axios.post('api/support', data);
        return response.data
    }
}