export default {
    async login(data, axios) {
        let response = await axios.post('api/login', data)
        return response.data
    },
    async signup(data, axios) {
        let response = await axios.post('api/signup', data)
        return response.data
    },
    async forgot(data, axios) {
        let response = await axios.post('api/user/forgot-password', data)
        return response.data
    },
    async checkCode(data, axios) {
        let response = await axios.post('api/user/check-code', data)
        return response.data
    },
    async resetPassword(data, axios) {
        let response = await axios.post('api/user/reset-password', data)
        return response.data
    },
    async getInfo(data, axios) {
        let response = await axios.post('api/user/profile', data)
        return response.data
    },
    async changePassword(data, axios){
        let response = await axios.post('api/user/change-password', data)
        return response.data
    },
    async update(data, axios) {
        let response = await axios.post('api/user/update-profile', data)
        return response.data
    },
    async postCv(data, axios){
        let response = await axios.post('/uploadFile',data)
        return response.data
    },
    async getCv(data, axios) {
        let response = await axios.get(`/downloadFile/${data}`)
        return response.data
    },
    async apply(data, axios) {
        let response = await axios.post('/api/jobApply', data)
        return response.data
    },
    async getApplyToJob(data, axios){
        const response = await axios.post('/api/job/get-list-apply-job', data)
        return response.data
      }
}