export const state = () => ({ data: {} });

export const getters = {
	getData: (state) => (key, $default = null) => {
		return state.data[key] || $default;
	},
};

export const mutations = {
	addData(state, { key, value }) {
		let tmp = {};
		tmp[key] = value;
		state.data = Object.assign({}, state.data, tmp);
	},
};

export const actions = {
	setData({ commit }, { key, value = null }) {
		commit("addData", { key: key, value: value });
	},
};